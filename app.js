'use-strict'

var spa = angular
	
	.module('web', [			// add name for your app (this should be same as in package.json && ng-app in index.html)
		'ngRoute',			    // add modules here and their js files in index.html to use them
		'ngMaterial'
	])

	.config([					    // creating a model to define screens for your app
		'$routeProvider',		// add Provider libraries to define state/url/theme of your screens
		'$mdThemingProvider',
		function (
			$rp,
			$mdt) {

				// defining screens here
				$rp.when('/news', {
					templateUrl: 'views/news.html',
					controller: 'newsCTRL',
					resolve: {
						news: ['API', function($api) {
							return $api.getNews();
						}]
					}
				}).otherwise({
					template: '<h3> Welcome to the News SPA. </h3><p> Please select a navigation point from the SideNav to be routed to your desired screen. </p>'
				});

				$rp.when('/create', {
					templateUrl: 'views/create.html',
					controller: 'createCTRL'
				});

				$rp.when('/about', {
					templateUrl: 'views/about.html',
					controller: 'aboutCTRL'
				});

				$rp.when('/faq', {
					templateUrl: 'views/faq.html',
					controller: 'faqCTRL'
				});

				// Extend the BLUE theme with a different color and make the contrast color black instead of white.
				// For example: raised button text will be black instead of white.
				var myPrimaryMap = $mdt.extendPalette('blue', {
				'500': '#82B1FF',
				'contrastDefaultColor': 'dark'
				});
				var myAccentMap = $mdt.extendPalette('blue', {
				'500': '#4D82CB',
				'contrastDefaultColor': 'light'
				});

				// Register the new color palette map with the name <code>blue</code>
				$mdt.definePalette('customPrimary', myPrimaryMap);
				$mdt.definePalette('customAccent', myAccentMap);

				$mdt.theme('default')
					.primaryPalette('customPrimary')
					.accentPalette('customAccent')
					.dark();
    			$mdt.enableBrowserColor({
				      theme: 'default', // Default is 'default'
				      palette: 'accent', // Default is 'primary', any basic material palette and extended palettes are available
				      hue: '200' // Default is '800'
				    });
		}
	]);