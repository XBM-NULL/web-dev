// Helper Class
spa.factory("H",[
	"$mdToast", '$mdSidenav',
	function ($toast, $sn) {

		function H() { 
		};

		H.prototype.printInfo = (TAG, text) => {
			this.printInfo(TAG, text,null);
		};
		H.prototype.printInfo = (TAG, text, more) => {
			// This will print all logs, uncomment for debugging only.
			console.log(TAG,text,more);
		};

		H.prototype.showSimpleToast = (text) => {
			$toast.show(
				$toast.simple()
				.textContent(text)
				.position('top right')
				.hideDelay(1500)
			);

		};

		H.prototype.openSideNav = () => {
			$sn('left').open();
		};

		H.prototype.closeSideNav = () => {
			$sn('left').close();
		};

		return new H();

	}
]);