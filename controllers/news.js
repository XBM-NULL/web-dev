'use-strict'
spa.controller('newsCTRL', [
	'$scope', '$location', 'newsData', 'news', 'H',
	function($scope, $location, $data, $news, $h) {

		$data.setData($news.data.arr);
		$scope.allNews = $data.get();

		var TAG = "NEWS CTRL::";

		$h.printInfo(TAG, "Controller Loaded!");
		$h.printInfo(TAG, $scope.allNews);

	}
]);